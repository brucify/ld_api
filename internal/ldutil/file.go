package ldutil

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func ReadKeyFromFile() string {
	fileName := "google_api_key"
	b, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Print(err)
	}
	str := string(b)
	//	fmt.Println(len(str))

	return strings.TrimSuffix(str, "\n")
}
