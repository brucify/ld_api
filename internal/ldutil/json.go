package ldutil

import (
	json "encoding/json"
)

func DecodeJson(str string) (j map[string]interface{}) {
	json.Unmarshal([]byte(str), &j)
	return j
}
