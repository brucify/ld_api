package ldhttp

import (
	"io/ioutil"
	"net/http"
	// "github.com/patrickmn/go-cache" // TODO add caching
	// "time"
)

type Params map[string]string

// TODO save HTTPResponse using the CacheKey
// type CacheKey struct {
//   method string
//   url string
//   params params
// }
//
// type HTTPResponse struct {
//   status_code string
//   body string
// }

func Request(method string, url string, params Params) (resStatus string, resBody string) {

	resStatus, resBody = doRequest(method, url, params)
	return resStatus, resBody

	// c := cache.New(5*time.Minute, 10*time.Minute) // TODO put this into a goroutine, for fetching the cache from
	//
	// k = CacheKey{method, url, params}
	// cached, cache_found := c.Get(k)
	// if cache_found {
	// 	return cached
	// } else {
	//   resStatus, resBody = doCallHTTP(method, url, params)
	//   response = HTTPResponse{resStatus, resBody}
	//   c.Set(key, response, cache.DefaultExpiration)
	// }

}

func doRequest(method string, url string, params Params) (resStatus string, resBody string) {
	client := &http.Client{}
	req, _ := http.NewRequest(method, url, nil)
	query := req.URL.Query()
	req.Header.Add("Accept", "application/json")

	for k, v := range params {
		query.Add(k, v)
	}
	req.URL.RawQuery = query.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return "", ""
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		resStatus = resp.Status
		resBody = string(body)

		//fmt.Println(resStatus)
		//fmt.Println(resBody)

		return resStatus, resBody
	}
}
