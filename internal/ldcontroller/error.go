package ldcontroller

import "github.com/gin-gonic/gin"

func errorResponse(ctx *gin.Context, status int, jsonResult interface{}) {
	err := errorMsg{
		Code:    status,
		Message: jsonResult,
	}
	ctx.JSON(status, err)
}

type errorMsg struct {
	Code    int         `json:"code" example:"400"`
	Message interface{} `json:"an additional message"`
}
