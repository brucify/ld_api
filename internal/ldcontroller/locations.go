package ldcontroller

import (
	"fmt"
	"net/http"

	ldhttp "ldapi/internal/ldhttp"
	"ldapi/internal/ldutil"

	"github.com/gin-gonic/gin"
)

const SERGELSTORG = "59.332335, 18.064159"
const GOOGLE_MAPS_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"

// GetLocations godoc
// @Summary Get all locations
// @Description Get locations after the search keyword within the given radius
// @Tags locations
// @Accept  json
// @Produce  json
// @Param keyword query string true "Keyword to search for"
// @Param radius query number false "Radius in meters. Max 50000 m"
// @Success 200
// @Failure 400 {object} ldcontroller.errorMsg
// @Failure 500 {object} ldcontroller.errorMsg
// @Failure 501 {object} ldcontroller.errorMsg
// @Router /locations [get]
func (c *Controller) GetLocations(ctx *gin.Context) {
	params := ldhttp.Params{
		"key":      ldutil.ReadKeyFromFile(),
		"location": SERGELSTORG,
		"radius":   ctx.Request.URL.Query().Get("radius"),
		"keyword":  ctx.Request.URL.Query().Get("keyword"),
		"type":     "bicycle_store",
	}
	resStatus, resBody := ldhttp.Request("GET", GOOGLE_MAPS_URL, params)
	jsonResult := ldutil.DecodeJson(resBody)

	fmt.Println("ld_location.go resStatus is:")
	fmt.Println(resStatus)

	switch resStatus {
	case "200 OK":
		ctx.JSON(http.StatusOK, jsonResult)
	default:
		errorResponse(ctx, http.StatusInternalServerError, jsonResult)
		return
	}
}
