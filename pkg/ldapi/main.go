package ldapi

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"

	ldcontroller "ldapi/internal/ldcontroller"

	_ "ldapi/pkg/ldapi/docs"
)

// @title ld_api
// @version 1.0
// @description This is a simple passthrough API for sending requests to Google Place API.

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8081
// @BasePath /api/v1
func Run() {
	engine := gin.Default()
	ctrl := ldcontroller.New()

	grp := engine.Group("/api/v1")
	{
		locations := grp.Group("/locations")
		{
			locations.GET("", ctrl.GetLocations)
		}
	}

	// TODO init the cache goroutine
	// go func() {
	//    c := cache.New(5*time.Minute, 10*time.Minute)
	// }()

	engine.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	engine.Run(":8081")
}
