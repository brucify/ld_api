# Use an official Golang as a parent image
FROM golang:1.16.3-alpine3.13

# Set the working directory to /app
WORKDIR /ldapi

# Copy the current directory contents into the container at /ld_api
COPY . /ldapi

# Install git and needed Go packages
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN go get -u github.com/swaggo/swag/cmd/swag
RUN go get -u github.com/swaggo/gin-swagger
RUN go get -u github.com/swaggo/gin-swagger/swaggerFiles
RUN go get -u github.com/alecthomas/template
RUN go get -u github.com/gin-gonic/gin
RUN go get -u github.com/satori/go.uuid

# Make port 8081 available to the world outside this container
EXPOSE 8081

# Run a few commands when the container launches
CMD ["swag", "init"]
CMD ["go", "run", "main.go"]
