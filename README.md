# LD API

A simple passthrough API for sending requests to Google Place API.

Endpoint:
GET /api/v1/locations List locations

First create a file called google_api_key with the generated Google API Key.

Running

```console
$ go run main.go
```

Building and running with Docker

```console
$ docker build -t ldapi:v1 .
$ docker run -p 8081:8081 ldapi:v1
```

The swagger documentation is ready at http://localhost:8081/swagger/index.html)
